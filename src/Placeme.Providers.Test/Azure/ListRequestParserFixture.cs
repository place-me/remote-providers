using System;

using FluentAssertions;

using Microsoft.Graph;

using NUnit.Framework;

using Placeme.Common.Generated.Base;
using Placeme.Providers.Azure.Helpers;

using Filter = Placeme.Common.Generated.Base.Filter;
using ListRequest = Placeme.Common.Generated.Base.ListRequest;
using Operator = Placeme.Common.Generated.Base.Operator;

namespace Placeme.Providers.Test.Azure;

[TestFixture]
public class ListRequestParserFixture
{
	[Test]
	public void CorrectlyParseEqualsFilter()
	{
		var filterDto = new Filter
		{
			Left = "subject",
			Right = "Test",
			Operator = Operator.Equals
		};
		var filterSet = new FilterSet();
		filterSet.Filters.Add(filterDto);
		var listRequest = new ListRequest()
		{
			FilterSet = filterSet
		};
		var filter = listRequest.ConvertToFilterString<Event>();
		Assert.That(filter, Is.EqualTo("subject eq 'Test'"));
	}

	[Test]
	public void CorrectlyParseGreaterThanFilter()
	{
		var filterDto = new Filter
		{
			Left = "start",
			Right = "2022-01-30T22:00:00Z",
			Operator = Operator.GreaterThan
		};
		var filterSet = new FilterSet();
		filterSet.Filters.Add(filterDto);
		var listRequest = new ListRequest()
		{
			FilterSet = filterSet
		};
		var filter = listRequest.ConvertToFilterString<Event>();
		Assert.That(filter, Is.EqualTo("start/dateTime gt '2022-01-30T22:00:00Z'"));
	}

	[Test]
	public void CorrectlyParseLessThanFilter()
	{
		var filterDto = new Filter
		{
			Left = "start",
			Right = "2022-01-30T22:00:00Z",
			Operator = Operator.LessThan
		};
		var filterSet = new FilterSet();
		filterSet.Filters.Add(filterDto);
		var listRequest = new ListRequest()
		{
			FilterSet = filterSet
		};
		var filter = listRequest.ConvertToFilterString<Event>();
		Assert.That(filter, Is.EqualTo("start/dateTime lt '2022-01-30T22:00:00Z'"));
	}

	[Test]
	public void CorrectlyParseGreaterOrEqualThanFilter()
	{
		var filterDto = new Filter
		{
			Left = "end",
			Right = "2022-01-30T22:00:00Z",
			Operator = Operator.GreaterThanOrEqual
		};
		var filterSet = new FilterSet();
		filterSet.Filters.Add(filterDto);
		var listRequest = new ListRequest()
		{
			FilterSet = filterSet
		};
		var filter = listRequest.ConvertToFilterString<Event>();
		Assert.That(filter, Is.EqualTo("end/dateTime ge '2022-01-30T22:00:00Z'"));
	}

	[Test]
	public void CorrectlyParseContainsFilter()
	{
		var filterDto = new Filter
		{
			Left = "subject",
			Right = "Test",
			Operator = Operator.Contains
		};
		var filterSet = new FilterSet();
		filterSet.Filters.Add(filterDto);
		var listRequest = new ListRequest()
		{
			FilterSet = filterSet
		};
		var filter = listRequest.ConvertToFilterString<Event>();
		Assert.That(filter, Is.EqualTo("contains(subject,'Test')"));
	}

	[Test]
	public void CorrectlyParseNegatedFilter()
	{
		var filterDto = new Filter
		{
			Left = "subject",
			Right = "Test",
			Operator = Operator.Equals,
			Negate = true
		};
		var filterSet = new FilterSet();
		filterSet.Filters.Add(filterDto);
		var listRequest = new ListRequest()
		{
			FilterSet = filterSet
		};
		var filter = listRequest.ConvertToFilterString<Event>();
		Assert.That(filter, Is.EqualTo("subject ne 'Test'"));
	}

	[Test]
	public void CorrectlyThrowExceptionsOnUnsupportedNegateFilter()
	{
		var filterDto = new Filter
		{
			Left = "subject",
			Right = "Test",
			Operator = Operator.Contains,
			Negate = true
		};
		var filterSet = new FilterSet();
		filterSet.Filters.Add(filterDto);
		var listRequest = new ListRequest()
		{
			FilterSet = filterSet
		};
		var action = () => listRequest.ConvertToFilterString<Event>();
		action.Should().Throw<NotSupportedException>();
	}

	[Test]
	public void CorrectlyThrowExceptionsOnUnsupportedTypeFilter()
	{
		var filterDto = new Filter
		{
			Left = "subject",
			Right = "Test",
			Operator = Operator.Contains,
			CompareFields = true
		};
		var filterSet = new FilterSet();
		filterSet.Filters.Add(filterDto);
		var listRequest = new ListRequest()
		{
			FilterSet = filterSet
		};
		var action = () => listRequest.ConvertToFilterString<Event>();
		action.Should().Throw<NotSupportedException>();
	}

	[Test]
	public void CorrectlyParseMultipleFiltersAnd()
	{
		var filterDto = new Filter
		{
			Left = "subject",
			Right = "Test",
			Operator = Operator.Equals
		};
		var filterDto2 = new Filter
		{
			Left = "subject",
			Right = "Test2",
			Operator = Operator.Equals
		};
		var filterSet = new FilterSet
		{
			LogicalOperator = LogicalOperator.And
		};
		filterSet.Filters.Add(filterDto);
		filterSet.Filters.Add(filterDto2);
		var listRequest = new ListRequest()
		{
			FilterSet = filterSet
		};
		var filter = listRequest.ConvertToFilterString<Event>();
		Assert.That(filter, Is.EqualTo("subject eq 'Test' and subject eq 'Test2'"));
	}

	[Test]
	public void CorrectlyParseMultipleFiltersOr()
	{
		var filterDto = new Filter
		{
			Left = "subject",
			Right = "Test",
			Operator = Operator.Equals
		};
		var filterDto2 = new Filter
		{
			Left = "subject",
			Right = "Test2",
			Operator = Operator.Equals
		};
		var filterSet = new FilterSet
		{
			LogicalOperator = LogicalOperator.Or
		};
		filterSet.Filters.Add(filterDto);
		filterSet.Filters.Add(filterDto2);
		var listRequest = new ListRequest()
		{
			FilterSet = filterSet
		};
		var filter = listRequest.ConvertToFilterString<Event>();
		Assert.That(filter, Is.EqualTo("subject eq 'Test' or subject eq 'Test2'"));
	}

	[Test]
	public void CorrectlySortByDate()
	{
		var sort = new Sort()
		{
			SortElement = "start",
			IsDescending = true
		};
		var listRequest = new ListRequest()
		{
			Sort = sort
		};
		var sortString = listRequest.ConvertToSortString<Event>();
		Assert.That(sortString, Is.EqualTo("start/dateTime desc"));
	}

	[Test]
	public void CorrectlySortIfNoSortGiven()
	{
		var listRequest = new ListRequest();
		var sortString = listRequest.ConvertToSortString<Event>();
		Assert.That(sortString, Is.EqualTo(""));
	}
}