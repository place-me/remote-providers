namespace Placeme.Providers.Models;

public enum RemoteRecurrenceWeekIndex
{
	First = 0,
	Second = 1,
	Third = 2,
	Fourth = 3,
	Last = 4
}