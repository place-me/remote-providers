using System;

using Placeme.Providers.Interfaces;

namespace Placeme.Providers.Models;

public class RemoteScheduleItem
{
	/// <summary>
	/// The date, time, and time zone that the corresponding event ends.
	/// </summary>
	public DateTime End { get; set; }

	/// <summary>
	/// The sensitivity of the corresponding event. True if the event is marked private, false otherwise. Optional.
	/// </summary>
	public bool IsPrivate { get; set; }

	/// <summary>
	/// The location where the corresponding event is held or attended from. Optional.
	/// </summary>
	public string Location { get; set; } = string.Empty;

	/// <summary>
	/// The date, time, and time zone that the corresponding event starts.
	/// </summary>
	public DateTime Start { get; set; }

	/// <summary>
	/// The availability status of the user or resource during the corresponding event.
	/// The possible values are: free, tentative, busy, oof, workingElsewhere, unknown.
	/// </summary>
	public RemoteFreeBusyStatus Status { get; set; }

	/// <summary>
	/// The corresponding event's subject line. Optional.
	/// </summary>
	public string? Subject { get; set; }
}