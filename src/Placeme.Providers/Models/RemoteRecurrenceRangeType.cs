namespace Placeme.Providers.Models;

public enum RemoteRecurrenceRangeType
{
	EndDate = 0,
	NoEnd = 1,
	Numbered = 2
}