using System;

namespace Placeme.Providers.Models;

public class RemoteRecurrenceRange
{
	public RemoteRecurrenceRangeType? Type { get; set; }
	public DateTime? EndDate { get; set; }
	public int? NumberOfOccurrences { get; set; }
}