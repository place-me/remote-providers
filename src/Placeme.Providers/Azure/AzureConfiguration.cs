using System;
using System.Collections.Generic;

namespace Placeme.Providers.Azure;

public class AzureConfiguration
{
	public static readonly string[] DefaultRoles = { "User.Read.All", "Directory.Read.All", "Place.Read.All" };
	public static readonly string[] DefaultScopes = { "https://graph.microsoft.com/.default" };

	public string? Tenant { get; set; }
	public string? ClientId { get; set; }
	public string? ClientSecret { get; set; }
	public IEnumerable<string> Scopes { get; set; } = Array.Empty<string>();
	public IEnumerable<string> RequiredRoles { get; set; } = Array.Empty<string>();
}