using Placeme.Providers.Interfaces;

namespace Placeme.Providers.Azure;

public class AzureUserGroup : IRemoteUserGroup
{
	public string? Id { get; set; }
	public string? Description { get; set; }
	public string? Name { get; set; }
}