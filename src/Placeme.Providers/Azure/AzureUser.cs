using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.Json.Serialization;

using Placeme.Providers.Interfaces;
using Placeme.Providers.Serialization;

namespace Placeme.Providers.Azure;

public class AzureUser : IRemoteUser
{
	public string? City { get; set; }
	public string? Country { get; set; }
	public string? Department { get; set; }
	public string? Division { get; set; }
	public string? Email { get; set; }
	public string? Firstname { get; set; }
	public string? Function { get; set; }

	[JsonConverter(typeof(InterfaceListConverter<IRemoteUserGroup, RemoteUserGroupConverter<AzureUserGroup>>))]
	public IEnumerable<IRemoteUserGroup> Groups { get; set; } = new Collection<IRemoteUserGroup>();
	public string? Id { get; set; }
	public string? Lastname { get; set; }
	public string? PhoneNumber { get; set; }
	public string? Street { get; set; }
	public string? ZipCode { get; set; }
}