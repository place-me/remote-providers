using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Placeme.Common.Application.Models;
using Placeme.Common.Generated.Base;
using Placeme.Providers.Models;

namespace Placeme.Providers.Interfaces;

public interface IRemoteProvider
{
	Task<IEnumerable<IRemoteUser>> GetUsersAsync(CancellationToken cancellationToken = default);
	Task<byte[]> GetPhotoAsync(string id, CancellationToken cancellationToken = default);
	Task<IEnumerable<RemoteAsset>> GetAssetsAsync(CancellationToken cancellationToken = default);
	Task<IRemoteEvent> GetEventAsync(string id, CancellationToken cancellationToken = default);
	Task<PaginatedList<IRemoteEvent>> ListEventsAsync(ListRequest listRequest, CancellationToken cancellationToken = default);

	Task<IRemoteEvent> CreateEventAsync(IRemoteEvent remoteEvent, CancellationToken cancellationToken = default);

	Task<IRemoteEvent> UpdateEventAsync(IRemoteEvent remoteEvent, RemoteEventUpdatableProperties propertiesToUpdate, CancellationToken cancellationToken = default);
	Task DeleteEventAsync(string id, CancellationToken cancellationToken = default);

	Task<IEnumerable<RemoteScheduleInformation>> GetFreeBusyAsync(string userId, IEnumerable<string> attendees, DateTime start, DateTime end,
		int interval = 30, CancellationToken cancellationToken = default);

	Task<IEnumerable<RemoteScheduleInformation>> GetFreeBusyAsync(IEnumerable<string> attendees, DateTime start, DateTime end,
		int interval = 30, CancellationToken cancellationToken = default);

	Task<bool> ValidateConfigurationAsync(string configurationJson, CancellationToken cancellationToken = default);

	Task<string> GenerateDefaultConfigurationAsync(string configurationJson,
		CancellationToken cancellationToken = default);

	Task<IEnumerable<IRemotePermission>> ReviewPermissionsAsync(CancellationToken cancellationToken = default);

	// Task UseUserAuthenticationAsync(string configurationJson, string accessToken,
	// 	CancellationToken cancellationToken = default);
	// Task UseApplicationAuthenticationAsync(string configurationJson, CancellationToken cancellationToken = default);
}