namespace Placeme.Providers.Interfaces;

public enum RemoteConnectionAuthType
{
	/// <summary>
	///     Authenticate on behalf of a user and work in its scope
	/// </summary>
	OnBehalf = 0,

	/// <summary>
	///     Authenticate as the application, most times this mode will provide less permissions
	/// </summary>
	Application = 1
}