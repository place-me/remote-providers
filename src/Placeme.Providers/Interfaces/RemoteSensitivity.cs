namespace Placeme.Providers.Interfaces;

public enum RemoteSensitivity
{
	Normal,
	Personal,
	Private,
	Confidential
}