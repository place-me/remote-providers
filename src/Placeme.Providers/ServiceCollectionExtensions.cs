using Microsoft.Extensions.DependencyInjection;

using Placeme.Providers.Interfaces;

namespace Placeme.Providers;

public static class ServiceCollectionExtensions
{
	public static IServiceCollection AddPlacemeProviders(this IServiceCollection services)
	{
		services.AddTransient<IRemoteProviderFactory, RemoteProviderFactory>();

		return services;
	}
}