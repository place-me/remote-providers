using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Placeme.Providers.Serialization;

public class InterfaceListConverter<T, TConverter> : JsonConverter<IEnumerable<T>>
	where TConverter : JsonConverter, new()
{
	public override bool CanConvert(Type typeToConvert)
	{
		return typeToConvert.IsAssignableTo(typeof(IEnumerable<T>));
	}

	public override IEnumerable<T> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
	{
		var internalOptions = new JsonSerializerOptions(options);
		internalOptions.Converters.Clear();
		internalOptions.Converters.Add(new TConverter());

		var result = JsonSerializer.Deserialize<List<T>>(ref reader, internalOptions);

		return result ?? (IEnumerable<T>)Array.Empty<T>();
	}

	public override void Write(Utf8JsonWriter writer, IEnumerable<T> value, JsonSerializerOptions options)
	{
		JsonSerializer.Serialize(writer, value, value.GetType());
	}
}