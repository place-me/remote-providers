﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Google.Protobuf.WellKnownTypes;

using Microsoft.Extensions.DependencyInjection;

using Placeme.Common.Generated.Base;
using Placeme.Providers.Azure;
using Placeme.Providers.Interfaces;

using Spectre.Console;

namespace Placeme.Providers.Debugger;

#pragma warning disable RCS1208 // Reduce 'if' nesting
internal static class Program
{
	public static DateTime RoundUp(this DateTime dt, TimeSpan d)
	{
		var delta = (d.Ticks - (dt.Ticks % d.Ticks)) % d.Ticks;
		return new DateTime(dt.Ticks + delta, dt.Kind);
	}

	private static void RenderTable<TEntity>(string[] columns, IList<TEntity> assets, Func<TEntity, string> firstColValuePredicate)
	{
		var table = new Table().BorderColor(Color.Green);
		foreach (var column in columns)
		{
			table.AddColumn($"[bold yellow]{column}[/]");
		}

		var props = typeof(TEntity).GetProperties().Where(p => p.PropertyType == typeof(string) || !p.PropertyType.GetInterfaces().Contains(typeof(IEnumerable))).ToList();

		for (var i = 0; i < assets.Count; i++)
		{
			var asset = assets[i];
			for (int j = 0; j < props.Count; j++)
			{
				var prop = props[j];
				var propValue = prop.GetValue(asset) ?? "-";
				var firstColValue = string.Empty;
				if (j == 0)
				{
					firstColValue = $"[blue]{firstColValuePredicate(asset)}[/]";
				}
				table.AddRow(firstColValue, prop.Name, propValue.ToString());
			}

			if (i < assets.Count - 1)
			{
				table.AddRow("");
			}
		}

		// Render the table to the console
		AnsiConsole.Write(table);
	}

	private static async Task Main(string[] args)
	{
		var services = new ServiceCollection();
		services.AddPlacemeProviders();
		var serviceProvider = services.BuildServiceProvider();

		var providerFactory = serviceProvider.GetService<IRemoteProviderFactory>();
		var clientId = Environment.GetEnvironmentVariable("AZURE_CLIENT_ID");
		var clientSecret = Environment.GetEnvironmentVariable("AZURE_CLIENT_SECRET");
		var tenant = Environment.GetEnvironmentVariable("AZURE_TENANT");

		var config = new AzureConfiguration
		{
			ClientId = clientId,
			ClientSecret = clientSecret,
			Tenant = tenant,
			Scopes = AzureConfiguration.DefaultScopes,
			RequiredRoles = AzureConfiguration.DefaultRoles
		};
		var azureProvider = providerFactory.GetAzureProvider(config, null);

		if (args.Contains("users"))
		{
			await azureProvider.GetUsersAsync();
		}

		if (args.Contains("places"))
		{
			var assets = (await azureProvider.GetAssetsAsync()).OrderBy(a => a.Name).ToList();
			RenderTable(new string[] { "Asset", "Key", "Value" }, assets, e => e.Name);
		}

		if (args.Contains("freebusy"))
		{
			var attendees = (await azureProvider.GetAssetsAsync()).Select(a => a.Email);
			var users = await azureProvider.GetUsersAsync();
			var user = users.Single(u => u.Firstname == "Lukas" && u.Lastname == "Läderach");
			var start = DateTime.Now.RoundUp(TimeSpan.FromMinutes(30)).AddDays(1);
			var end = start.AddHours(2);
			var remoteSchedule = await azureProvider.GetFreeBusyAsync(user.Id!, attendees, start, end, (int)(end - start).TotalMinutes);

			var fmt = new CultureInfo("de-CH").DateTimeFormat;

			foreach (var remoteScheduleInformation in remoteSchedule)
			{
				RenderTable(new string[] { "Schedule", "Key", "Value" },
				 remoteScheduleInformation.ScheduleItems.ToList(), _ => remoteScheduleInformation.ScheduleId);
			}
		}

		if (args.Contains("events"))
		{
			var remoteEvent = new AzureEvent
			{
				Start = DateTime.Now.AddHours(1),
				End = DateTime.Now.AddHours(2),
				Attendees = new List<AzureAttendee>
				{
					new()
					{
						Email = "AdeleV@placeyou.onmicrosoft.com",
						Type = RemoteAttendeeType.Optional
					},
					new()
					{
						Email = "ConferenceRoom1@placeyou.onmicrosoft.com",
						Type = RemoteAttendeeType.Resource
					}
				}
			};
			var azureEvent = await azureProvider.CreateEventAsync(remoteEvent);
			await azureProvider.ListEventsAsync(new ListRequest());
			var loadedEvent = await azureProvider.GetEventAsync(azureEvent.Id);
			loadedEvent.Start = azureEvent.Start.AddDays(1);
			loadedEvent.End = azureEvent.End.AddDays(1);
			var fieldsToUpdate = RemoteEventUpdatableProperties.Start | RemoteEventUpdatableProperties.End;
			await azureProvider.UpdateEventAsync(loadedEvent, fieldsToUpdate);
			await azureProvider.DeleteEventAsync(loadedEvent.Id);
		}
	}
}