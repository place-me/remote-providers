# Placeme.Providers

This library contains access to remote users and assets.

The following providers are implemented:
- Microsoft Azure AD
- Microsoft Exchange

## Debugging

Debugging is straight forward: Add a `.env` file with the following variables

```env
AZURE_CLIENT_ID={Azure App Registration Id}
AZURE_CLIENT_SECRET={Azure App Registration Secret}
AZURE_TENANT={Azure Tenant}
```

Once this file is available at root you can debug the application via `F5`. There are two launch tasks available, one for the user endpoint and one for the resource endpoint.